<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_detail', function (Blueprint $table) {
            $table->id();
            $table->string('UserPublicId', 250);
            $table->string('name');
            $table->string('email')->unique();
            $table->string('phone_number');
            $table->string('status');
            $table->string('profile_picture', 250);
            $table->string('affiliate_code');
            $table->string('is_verified');
            $table->string('instagram');
            $table->string('facebook');
            $table->string('twitter');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_detail');
    }
};
